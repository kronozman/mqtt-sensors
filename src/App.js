import React from 'react';
import Widgets from './components/Widgets';
import Plot from './components/Plot';
import DatePicker from './components/DatePicker';


var sensors = [
 { text:'Temperature', tag:'temperature', unit:'\u00B0C', digits:'1'},
 { text:'Humidity', tag:'humidity', unit:'%', digits:'1'},
 { text:'Light', tag:'light', unit:'lux', digits:'1'}
]
function App() {
  return (
    <div>
       <Widgets monitors={sensors} />
       <Plot />
       <DatePicker />
       <div>{process.env.REACT_APP_NAME} v{process.env.REACT_APP_VERSION} </div>
    </div>
  )
  }


export default App;
