import React, {Component } from 'react';
import { ResponsiveContainer, Tooltip, Area, CartesianGrid, AreaChart, XAxis, YAxis } from 'recharts';
import CircularProgress from '@material-ui/core/CircularProgress';
import moment from 'moment';

const API = process.env.REACT_APP_REST_URL

export default class Plot extends Component {

    constructor(props) {
        super()
        this.state = {
            data: null,
            loading: true,
            message: null
        }
        setTimeout( () => this.tick(), Math.floor(Math.random() * 100) + 100)
    }

    componentDidMount() {
        this.mounted = true
        this.intervalID = setInterval(() => this.tick(), 30000)
    }

    componentWillUnmount() {
        this.mounted = false
        clearInterval(this.intervalID)
    }

    tick() {
        let query = API + 'temperature_history'
        fetch(query)
        .then(response => response.json())
        .then(result => {if (this.mounted) 
            this.setState({data:result, loading:false}) }
        )
        .catch(error => {if(this.mounted) 
            this.setState({loading: true, message: error})}
        )
    }

    render(){
        const { data } = this.state

        if(this.state.message){
            return <div>#ERR</div>
        }
        if(this.state.loading){
            return <div><CircularProgress /></div>
        }

        // null values
        if(Object.entries(data).lenght === 0){
            return null
        }
        
        return (
            <div style = {{ width: '100%', height: 200} }>
                <ResponsiveContainer>
                    <AreaChart
                        data = {data} 
                        margin = {{left:10, top:10}}
                    >
                        <Tooltip/>
                        <CartesianGrid strokeDasharray="3 3"></CartesianGrid>
                        <YAxis dataKey= 'value'></YAxis>
                        <XAxis dataKey = 'timestamp' minTickGap={30}
                            tickFormatter = { (unitTime) => moment(unitTime).format('HH:mm')} />
                        <Area dataKey="value" />
                    </AreaChart>
            </ResponsiveContainer>
            </div>
            
                   
        )
    }
}